import React from "react";
import ReactDOM from "react-dom";
import {Header, MobileLayout, FeatureList} from '../containers';

class App extends React.Component {
  render() {
    return <MobileLayout >
    	<Header />
    	<FeatureList />
    </MobileLayout>;
  }
}

const home = document.getElementById("home");

ReactDOM.render(<App/>, home);
