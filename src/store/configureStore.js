import {createStore, applyMiddleware, compose} from 'redux';
import { apiMiddleware } from 'redux-api-middleware';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import rootReducer from '../reducers/index';

let createStoreWithMiddleware;
if(_DEVELOPMENT_) {
	createStoreWithMiddleware = compose(
		applyMiddleware(thunkMiddleware, apiMiddleware),
		applyMiddleware(createLogger()),
	)(createStore);
} else {
	createStoreWithMiddleware = compose(
		applyMiddleware(thunkMiddleware, apiMiddleware)
	)(createStore);
}

export default function configureStore(initialState) {
    const store = createStoreWithMiddleware(rootReducer, initialState);

    return store;
}