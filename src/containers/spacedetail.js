import React from "react";
import Space from '../components/space';
import {SpaceDetailHead} from '../components/spacedetail';
import SPACES from '../data/spaces';
import _, {clone} from 'lodash';

const CATETORIES = [{
	name: 'description',
	name_chn: '介绍',
	content: (space) => !!space.spaceName && <div className={space.spaceName}>{space.spaceName}</div>
}, {
	name: 'capacity',
	name_chn: "可以容纳",
	content: (space) => !!space.maxCapacity ? <div className="capacity">{`${space.minCapacity}人－${space.maxCapacity}人`}</div> : <div className="capacity">{`${space.minCapacity}人`}</div>
}, {
	name: "amenities",
	name_chn: "设施",
	content: (space) => !!space.amenities && <div className="amenities">{space.amenities.map((a, k) => <li key={k} className={`${a.name} item`}>{a.name_chn}</li>)}</div>
}, {
	name: "time",
	name_chn: "开发时间",
	content: () => <div><li>周一到周五</li><li>9AM 至 6PM</li></div>
}, {
	name: "address",
	name_chn: "地址",
	content: (space) => !!space.address && <div>{space.address}</div>
}];

export default class SpaceDetail extends React.Component {
	state = {
		space: {}
	}

	constructor(props) {
		super(props);
		this.classname = "SpaceDetail";
	}

	componentDidMount() {
		this.setState({
	      // route components are rendered with useful information, like URL params
	      space: this.findSpaceById(this.props.params.spaceId)[0]
	    })
	}

	render() {
		const {
			space
		} = this.state;
		if(space) {
			const cn = this.classname;
			return <div className={`${cn}`} >
				<SpaceDetailHead 
					id={space.id}
					imgUrl={space.imgUrl}
					isAvailable={space.isAvailable}
					spaceName={space.spaceName}
					rate={space.rate}
					minCapacity={space.minCapacity}
					maxCapacity={space.maxCapacity}
					distance={space.distance}
				/>
				{this.buildContent()}
			</div>;

		} else {
			return null;
		}
	}

	buildContent() {
		const {
			space
		} = this.state;
		const cn = this.classname;
		const contentArr = clone(CATETORIES);
		return contentArr.map((c, k) => {
			const _buildContent = c.content;
			return <div className={`${cn}-section`} key={k}>
				<div className={`${cn}-section-name`} >{c.name_chn}</div>
				<div className={`${cn}-section-content`}>{_buildContent(space)}</div>
			</div>;
		});
	}


	findSpaceById(id) {
		return _.filter(SPACES, {id : parseInt(id, 10)});
	}
}