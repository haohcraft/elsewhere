"use strict";
import SpaceList from './spacelist';
import SpaceDetail from './spacedetail';
import MobileLayout from './mobile';
import Header from "./header";

module.exports = {
	Header,
	MobileLayout,
	SpaceList,
	SpaceDetail
};