import React from "react";

export default class MobileLayout extends React.Component {

	render() {
		return <div style={{maxWidth: '375px'}}>
			{this.props.children}
		</div>;
	}
}
