import React from "react";
import Space from '../components/space';

import SPACES from '../data/spaces';

export default class SpaceList extends React.Component {

	render() {
		
		return <div>
			{this.buildSpaceList()}
		</div>;
	}

	buildSpaceList() {
		const spaces = SPACES;
		return spaces.map((s, k) => <Space 
			key={k}
			id={s.id}
			imgUrl={s.imgUrl}
			isAvailable={s.isAvailable}
			spaceName={s.spaceName}
			rate={s.rate}
			minCapacity={s.minCapacity}
			maxCapacity={s.maxCapacity}
			distance={s.distance}
		/>);
	}
}
