import React from "react";
import {Router, Route, IndexRoute} from 'react-router'
import { Provider } from 'react-redux'
import {MobileLayout, Header, SpaceList, SpaceDetail} from "./containers";
import configureStore from './store/configureStore';
import { createHistory } from 'history'

require('./assets/styles/base');

const store = configureStore();
const history = createHistory();

class Detail extends React.Component {
	render() {
		return <SpaceDetail params={this.props.params}/>;
	}
}

class List extends React.Component {
	render() {
		return <SpaceList />;
	}
}

class Home extends React.Component {
	render() {
		return <MobileLayout >
			<Header />
			{this.props.children}
		</MobileLayout>;
	}
}

export default class App extends React.Component {
  render() {
    return <Provider store={store}>
	    <Router history={history}>
			<Route path="/" component={Home}>
				<IndexRoute component={List}/>
				<Route path="/space/:spaceId" component={Detail}/>
			</Route>
		</Router>
	</Provider>
  }
}