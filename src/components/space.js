"use strict";

import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router'

require('../assets/styles/components/space');

export default class Space extends Component {

	static propTypes = {
		imgUrl: PropTypes.string.isRequired,
		isAvailable: PropTypes.bool.isRequired,
		spaceName: PropTypes.string.isRequired,
		minCapacity: PropTypes.number.isRequired,
		maxCapacity: PropTypes.number.isRequired,
		rate: PropTypes.number.isRequired,
		distance: PropTypes.number
	}

	static defaultProps= {
		imgUrl: "http://e.36krcnd.com/nil_class/9c302b76-8e96-47ed-af57-9ba0c0881e88/5417d7ec20303.jpg!slider",
		isAvailable: true,
		spaceName: "中鼓楼大街",
		rate: 70,
		minCapacity: 3,
		maxCapacity: 7,
		distance: 5.8,
		unit: "元／时"
	}

	constructor(props) {
		super(props);
		this.classname = "Space";
		this.customClass = null;
	}

	render() {
		const {
			id,
			imgUrl,
			isAvailable,
			rate,
			unit
		} = this.props;
		const cn = this.classname;
		return <div className={`${cn} ${this.customClass} container`} style={{backgroundImage: `url(${imgUrl})`}}>
			<Link to={`/space/${id}`}>
				<div className={`${cn}-description-bg`}></div>
				<div className={`${cn}-description`}>
					<div className={`${cn}-description-left`}>
						{!!isAvailable ? <span className="available">现在可用</span> : <span className="busy">忙碌</span>}
						{this.buildContent()}
					</div>
					<div className={`${cn}-description-right`}>
						<div className={`${cn}-description-right-rate`}>{`${rate}${unit}`}</div>
					</div>
				</div>
			</Link>
		</div>;
	}

	buildContent() {
		const {
			spaceName,
			distance
		} = this.props;
		const cn = this.classname;
		return <div>
			<div className={`${cn}-description-left-name`}>{spaceName}</div>
			<div className={`${cn}-description-left-content`}>
				<div>{distance} 公里&nbsp;|&nbsp;</div>
				{this.buildCapacity()}
			</div>
		</div>;
	}

	buildCapacity() {
		const {minCapacity, maxCapacity} = this.props;
		return !!maxCapacity ? <div className="capacity">{`${minCapacity}人－${maxCapacity}人`}</div> : <div className="capacity">{`${minCapacity}人`}</div>;
	}
}



