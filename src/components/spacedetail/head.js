import React, {PropTypes} from 'react';
import SpaceBase from '../space';

require('../../assets/styles/components/spacedetail/head');

export default class SpaceDetailHead extends SpaceBase {
	constructor(props) {
		super(props);
		this.customClass = "SpaceDetailHead";
	}
	buildContent() {
		return null;
	}
}