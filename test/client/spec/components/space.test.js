/**
 * Client tests
 */
import React from "react";
import Component from "src/components/space";
// Use `TestUtils` to inject into DOM, simulate events, etc.
// See: https://facebook.github.io/react/docs/test-utils.html
import TestUtils from "react-addons-test-utils";

describe("components/space", () => {

  it("has a classname Space", () => {
    const rendered = TestUtils.renderIntoDocument(<Component />);
    expect(rendered.classname).to.equal("Space");
  });
});