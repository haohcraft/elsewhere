/*global document:false*/
import ReactDOM from "react-dom";
import React from "react";
import App from '../src';

const home = document.getElementById("home");

ReactDOM.render(<App/>, home);
